#!/usr/bin/env perl6

use v6;
use lib $*PROGRAM-NAME.IO.dirname.IO.abspath ~ '/' ~ 'lib';	#### so we need to be in the right dir if we're running this... fix later.
use Hardcode:auth<jamesneko>;

my $HARDCODE_HOME = $*PROGRAM-NAME.IO.dirname.IO.abspath ~ '/';
my $HARDCODE_PAGES = $*PROGRAM-NAME.IO.dirname.IO.abspath ~ '/pages/';

sub MAIN {
	note "Hardcode installed in: $HARDCODE_HOME";
	my @page-file-paths = $HARDCODE_PAGES.IO.dir(test => /'.p6' $/);
	note "Pages available: ", @page-file-paths».basename;
	for @page-file-paths -> $page-path {
		note "Loading: $page-path";
		page-load($page-path);
	}

	message "MAIN: We're done now. This is a message from main, after all the pages have ran.";
}
