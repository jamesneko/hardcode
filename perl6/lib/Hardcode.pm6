#
# Hardcode - A big mess of monkeypatchable code for Hardcode's pages to hack on.
#
# There is probably a much much nicer way of doing this monkeypatching in P6, but I don't know what it is yet.
#
unit module Hardcode:auth<jamesneko>;

use v6;
use MONKEY-SEE-NO-EVAL;		# Because in any other context, arbitrary code execution is BAD!


# Where we keep all our core functionality:
our %API =
		page-load => sub ($path) {
			my $page-contents = $path.IO.slurp;
			EVAL $page-contents;
		},
		message => sub {
			say "BLAH: ", @_;
		},
	;


sub api-replace(Str $fnname, &sub) {
	my &old = %API{$fnname};
	%API{$fnname ~ ".old"} = &old;
	%API{$fnname} = &sub;
}


sub api-hook-after(Str $fnname, &sub) {
	my &old = %API{$fnname};
	%API{$fnname ~ ".old"} = &old;
	%API{$fnname} = sub { &old(@_); &sub(@_); };
}


# ---- CONVENIENCE FUNCTIONS ----

sub page-load is export {
	%API<page-load>(@_);
}

sub message is export {
	%API<message>(@_);
}
