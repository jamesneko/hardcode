# This page is run within the Hardcode module, so we don't need to `use` it to get at its functions.

use NCurses;

my $win = initscr() or die "Failed to initialise NCurses!";

printw("Hello NCurses World!");
mvaddstr(5, 10, "Press any key to exit (eventually)...");
nc_refresh;

my $keycode = -1;
while $keycode < 0 {
	$keycode = getch();
}

printw("\nYou pressed keycode " ~ $keycode ~ "!");
nc_refresh;
sleep 5;

endwin;

