# This page is run within the Hardcode module, so we don't need to `use` it to get at its functions.

say "This is the test page.";
message("This is a message from the test page.");

api-replace('message', sub { say "NYAN: ", @_ });

message("This is a message from the test page after overriding things maybe.");
