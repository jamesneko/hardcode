#!/usr/bin/env perl
use warnings;
use strict;
use v5.16;

use IO::Socket::INET;


my ($SOCKET, $CLIENT_SOCKET);

sub init
{
  $SOCKET = IO::Socket::INET->new(
    LocalHost => '127.0.0.1',
    LocalPort => '2323',
    Proto => 'tcp',
    Listen => 1,  # Not forking or anything, just doing one client connection in our main loop.
    Reuse => 1,
  ) or die "ERROR in Socket Creation : $!\n";
  say STDERR "Waiting for client connection on port 2323";
}


sub mainloop
{
  # Check for incoming connection - BLOCKING ATM
  if ( ! defined $CLIENT_SOCKET) {
    $CLIENT_SOCKET = $SOCKET->accept();
    my $peer_address = $CLIENT_SOCKET->peerhost();
    my $peer_port = $CLIENT_SOCKET->peerport();
    say STDERR "New connection from $peer_address:$peer_port";
    print $CLIENT_SOCKET "Welcome to Hardcode.\nCommand?\n> ";
  }
  
  my $command = readline $CLIENT_SOCKET;
  chomp $command;
  say STDERR "Received command: $command";
  print $CLIENT_SOCKET "\n> ";
}


sub main
{
  init();
  while (1) {
    mainloop();
  }
}


main();
