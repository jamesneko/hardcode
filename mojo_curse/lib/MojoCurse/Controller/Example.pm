package MojoCurse::Controller::Example;
use Mojo::Base 'Mojolicious::Controller';

# This action will render a template
sub index {
  my $self = shift;

  # Render template "example/index.html.ep" with no other variables set
  $self->render();
}

1;
