package MojoCurse::Controller::Websocket;
use Mojo::Base 'Mojolicious::Controller';
use Mojo::Log;
use DateTime;

my $log = Mojo::Log->new;
my $clients = {};

sub echo {
  my $self = shift;

  $log->debug(sprintf 'Client connected: %s', $self->tx);
  my $id = sprintf "%s", $self->tx;
  $clients->{$id} = $self->tx;

  $self->on(message => sub {
    my ($self, $msg) = @_;

    my $dt = DateTime->now(time_zone => 'Australia/Sydney');

    for (keys %$clients) {
      $clients->{$_}->send({json => {
        hms  => $dt->hms,
        text => $msg,
      }});
    }
  });

  $self->on(finish => sub {
    $log->debug('Client disconnected');
    delete $clients->{$id};
  });
}

1;
