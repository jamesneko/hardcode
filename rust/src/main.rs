use warp::Filter;

mod websocket;


#[tokio::main]
async fn main() {
  // GET /hello/warp => 200 OK with body "Hello, warp!"
  let hello = warp::path!("hello" / String)
      .map(|name| {
        println!("/hello/{} fired", name);
        format!("Hello, {}!", name)
      });

  // GET /goodbye/warp => 200 OK with body "Goodbye, warp!"
  let goodbye = warp::path!("goodbye" / String)
      .map(|name| {
        println!("/goodbye/{} fired", name);
        format!("Goodbye, {}!", name)
      });
  
  // WS /echo
  let echo = warp::path("echo")
      .and(warp::ws())
      .and_then(websocket::ws_handler);

  // Serve index.html from /
  let public = warp::get()
      .and(warp::path::end())
      .and(warp::fs::file("public/index.html"));

  // combine routes and serve
  let routes = hello
      .or(echo)
      .or(goodbye)
      .or(public)
      .with(warp::cors().allow_any_origin());
  println!("serving routes...");
  warp::serve(routes)
      .run(([127, 0, 0, 1], 3030))
      .await;
}
