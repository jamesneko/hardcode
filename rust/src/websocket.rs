use warp::{Reply};
use warp::ws::{Message, WebSocket};
use warp::Rejection;
use futures::{FutureExt, StreamExt};
use tokio::sync::mpsc;

type Result<T> = std::result::Result<T, Rejection>;

pub async fn ws_handler(ws: warp::ws::Ws) -> Result<impl Reply> {
  println!("ws_handler fired");
  Ok(ws.on_upgrade(
    move |socket| {
      println!("ws_handler on_upgrade fired");
      client_connection(socket)
    } 
  ))
}


pub async fn client_connection(ws: WebSocket) {
  let (client_ws_sender, mut client_ws_rcv) = ws.split();
  let (client_sender, client_rcv) = mpsc::unbounded_channel();
  
  tokio::task::spawn(client_rcv.forward(client_ws_sender).map(|result| {
    if let Err(e) = result {
      eprintln!("error sending websocket msg: {}", e);
    }
  }));

  println!("connected");

  while let Some(result) = client_ws_rcv.next().await {
    let msg = match result {
      Ok(msg) => msg,
      Err(e) => {
        eprintln!("error receiving ws message: {}", e);
        break;
      }
    };
    client_msg(msg).await;
  }

  println!("disconnected");
}


async fn client_msg(msg: Message) {
  println!("received message : {:?}", msg);
}
